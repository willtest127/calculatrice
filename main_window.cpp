#include <iostream>
#include <libsx.h>
#include <fstream>
#include <cstring>
#include <string>
#include "data.hpp"
#include "callbacks.hpp"

#define TAILLEZONESAISIE 130

void init_display(int argc, char **argv, void *d)
{

    // MakeButton et SetWidgetPos,
    Widget Saisie,
        Bchiffre0,
        Bchiffre1,
        Bchiffre2,
        Bchiffre3,
        Bchiffre4,
        Bchiffre5,
        Bchiffre6,
        Bchiffre7,
        Bchiffre8,
        Bchiffre9,
        Bdiv,   //Diviser
        Bmul,   //Multiplier
        Bsous,  //Soustraire
        Bplus,  //Addition
        Bpoint, // .
        Bmod,   // %
        Bdl,    // Delete
        Bmplus,
        Bcl, // Clear
        Bpuiss,
        Bmr,
        Bmc,
        Benter;

    Saisie = MakeStringEntry(NULL, TAILLEZONESAISIE, NULL, d);

    Bchiffre7 = MakeButton("7", NULL, d);
    Bchiffre8 = MakeButton("8", NULL, d);
    Bchiffre9 = MakeButton("9", NULL, d);
    Bdiv = MakeButton("/", NULL, NULL);
    Bdl = MakeButton("Dl", NULL, NULL);
    Bmplus = MakeButton("M+", NULL, NULL);

    Bchiffre4 = MakeButton("4", NULL, d);
    Bchiffre5 = MakeButton("5", NULL, d);
    Bchiffre6 = MakeButton("6", NULL, d);
    Bmul = MakeButton("x", NULL, NULL);
    Bcl = MakeButton("Cl", NULL, NULL);
    Bmr = MakeButton("MR", NULL, NULL);

    Bchiffre1 = MakeButton("1", NULL, d);
    Bchiffre2 = MakeButton("2", NULL, d);
    Bchiffre3 = MakeButton("3", NULL, d);
    Bsous = MakeButton("-", NULL, NULL);
    Bpuiss = MakeButton("^2", NULL, NULL);
    Bmc = MakeButton("MC", NULL, NULL);

    Bchiffre0 = MakeButton("0", NULL, d);
    Bpoint = MakeButton(".", NULL, NULL);
    Bmod = MakeButton("%", NULL, NULL);
    Bplus = MakeButton("+", NULL, NULL);
    Benter = MakeButton("Enter", NULL, NULL);

    SetWidgetPos(Bchiffre7, PLACE_UNDER, Saisie, NO_CARE, NULL);
    SetWidgetPos(Bchiffre8, PLACE_UNDER, Saisie, PLACE_RIGHT, Bchiffre7);
    SetWidgetPos(Bchiffre9, PLACE_UNDER, Saisie, PLACE_RIGHT, Bchiffre8);
    SetWidgetPos(Bdiv, PLACE_UNDER, Saisie, PLACE_RIGHT, Bchiffre9);
    SetWidgetPos(Bdl, PLACE_UNDER, Saisie, PLACE_RIGHT, Bdiv);
    SetWidgetPos(Bmplus, PLACE_UNDER, Saisie, PLACE_RIGHT, Bdl);

    SetWidgetPos(Bchiffre4, PLACE_UNDER, Bchiffre7, NO_CARE, NULL);
    SetWidgetPos(Bchiffre5, PLACE_UNDER, Bchiffre8, PLACE_RIGHT, Bchiffre4);
    SetWidgetPos(Bchiffre6, PLACE_UNDER, Bchiffre9, PLACE_RIGHT, Bchiffre5);
    SetWidgetPos(Bmul, PLACE_UNDER, Bdiv, PLACE_RIGHT, Bchiffre6);
    SetWidgetPos(Bcl, PLACE_UNDER, Bdl, PLACE_RIGHT, Bmul);
    SetWidgetPos(Bmr, PLACE_UNDER, Bmplus, PLACE_RIGHT, Bcl);

    SetWidgetPos(Bchiffre1, PLACE_UNDER, Bchiffre4, NO_CARE, NULL);
    SetWidgetPos(Bchiffre2, PLACE_UNDER, Bchiffre5, PLACE_RIGHT, Bchiffre1);
    SetWidgetPos(Bchiffre3, PLACE_UNDER, Bchiffre6, PLACE_RIGHT, Bchiffre2);
    SetWidgetPos(Bsous, PLACE_UNDER, Bmul, PLACE_RIGHT, Bchiffre3);
    SetWidgetPos(Bpuiss, PLACE_UNDER, Bcl, PLACE_RIGHT, Bsous);
    SetWidgetPos(Bmc, PLACE_UNDER, Bmr, PLACE_RIGHT, Bpuiss);

    SetWidgetPos(Bchiffre0, PLACE_UNDER, Bchiffre1, NO_CARE, NULL);
    SetWidgetPos(Bpoint, PLACE_UNDER, Bchiffre2, PLACE_RIGHT, Bchiffre0);
    SetWidgetPos(Bmod, PLACE_UNDER, Bchiffre3, PLACE_RIGHT, Bpoint);
    SetWidgetPos(Bplus, PLACE_UNDER, Bsous, PLACE_RIGHT, Bmod);
    SetWidgetPos(Benter, PLACE_RIGHT, Bplus, PLACE_UNDER, Bpuiss);

    GetStandardColors();
    ShowDisplay();
}

int main(int argc, char **argv)
{

    if (OpenDisplay(argc, argv) == 0)
    {
        std::cerr << "Erreur : Impossible de lancer le graphique" << std::endl;
        return 1;
    }

    init_display(argc, argv, NULL);
    MainLoop();
    return 0;
}
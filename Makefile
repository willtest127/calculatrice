#
# Variables par défaut (surcharge)
#
CXXFLAGS = -std=c++11 -Wall -g -O0 
#
# Variables utilisateur
#
SRC = main_window.cpp

#INC = ConfigParser.hpp
OBJ = $(SRC:.cpp=.o)

%: %.o
	$(LINK.cc) -o $@ $^

test: $(OBJ)
	g++ -o $@ $^ -lsx

ConfigParser.o: main_window.cpp main_window.hpp
ConfigParserTest.o: mainTest.cpp mainTest.hpp catch.hpp

#Person.o: Person.cpp Person.hpp
#PersonTest.o: PersonTest.cpp Person.hpp catch.hpp
#Fraction.o: Fraction.cpp Fraction.hpp 
#FractionTest.o: FractionTest.cpp Fraction.hpp catch.hpp

#
# Cibles habituelles
#
cleaner: clean
	rm -f test

clean:
	rm -f *.o 
